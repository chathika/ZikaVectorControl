/**	
 * 	Copyright (C) <2016>  <Ezequiel Gioia, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author ezequielgioia
*/
package zikaVectorControl;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import repast.simphony.engine.environment.RunListener;
import repast.simphony.essentials.RepastEssentials;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.continuous.AbstractContinuousSpace.PointHolder;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.ui.RSApplication;
import repast.simphony.util.ContextUtils;
import repast.simphony.context.*;

public class LogListener implements RunListener {

	private long startTime;
	private TimeWatch watch;
	private Context context;
	
	public LogListener(Context context) {
		this.context = context;
	}
	
	@Override
	public void stopped() {
		double tick = RepastEssentials.GetTickCount();
		System.out.println("stopped at " + tick);
		
		//double estimatedTime = (System.currentTimeMillis() - this.startTime) / 1000.0;
		
		long estimatedTime = watch.time(TimeUnit.SECONDS);
		System.out.println("Elapsed time: " + estimatedTime + " seconds.");
	}

	@Override
	public void paused() {
		double tick = RepastEssentials.GetTickCount();
		System.out.println("paused at " + tick);
	
		Grid<Object> grid = (Grid<Object>)context.getProjection("Grid");
		
		HashMap<GridPoint, Integer> byLocation = new HashMap<GridPoint, Integer>();
		HashMap<String, Integer> byType = new HashMap<String, Integer>();
		
		for(Object obj : grid.getObjects()) {
			GridPoint location = grid.getLocation(obj);
			String type = obj.getClass().getName();
			
			// increments location counter
			if (!byLocation.containsKey(location))
				byLocation.put(location, 0);
			
			byLocation.put(location, byLocation.get(location) + 1);
			
			// increments type counter
			if (!byType.containsKey(type))
				byType.put(type, 0);
			
			byType.put(type, byType.get(type) + 1);
			
		}
		
		// export to csv files
		CsvExport.exportTypesToCSV(byType);
		CsvExport.exportLocationsToCSV(byLocation);
		System.out.println("Grid size: " + grid.size());
	}

	@Override
	public void started() {
		this.startTime = System.currentTimeMillis(); 
		//double tick = RepastEssentials.GetTickCount();
		
		watch = TimeWatch.start();
				
		//System.out.println("started at " + tick);
	}

	@Override
	public void restarted() {
		//double tick = RepastEssentials.GetTickCount();
		//System.out.println("restarted at " + tick);
		
	}

}
