package zikaVectorControl;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.security.Policy.Parameters;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.filechooser.FileFilter;

import com.jidesoft.dialog.JideOptionPane;

import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;
import repast.simphony.ui.RSApplication;
import repast.simphony.userpanel.ui.UserPanelCreator;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.zones.AttractionZone;

public class UserPanel implements UserPanelCreator{
	
	static Context context;
	String newMapPath;
	@Override
	public JPanel createPanel() {
		JPanel panel1 = new JPanel();
		SpringLayout layout = new SpringLayout();
		panel1.setLayout( layout);
		
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
		DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
		decimalFormat.setGroupingUsed(false);
		
		JLabel logo = new JLabel(new ImageIcon("images/20131022_CAS_Logo.jpg"));
		JLabel loadMapFromFileLabel = new JLabel("-----Load Map From File-----");
		JFileChooser mapFileLocation = new JFileChooser();
		JTextField houseIndexField1 = new JFormattedTextField(decimalFormat);
		JLabel selectNewMapLabel = new JLabel("Selected Map: ");
		JLabel selectedMapLabel = new JLabel();
		JLabel setHouseIndexLabel1 = new JLabel();
		JButton selectMapButton = new JButton("Select New Map");
		JButton changeMapButton = new JButton("Apply New Scenario");
		
		houseIndexField1.setColumns(5);
		
		//Manual map generation controls
		JLabel manualMapGenerationLabel = new JLabel("-----Generate Map Manually-----");
		JLabel setHouseIndexLabel2 = new JLabel();
		JTextField houseIndexField2 = new JTextField(10);
		JLabel proximityInputLabel = new JLabel("Enter Proximity");
		JTextField proximityInputField = new JFormattedTextField(numberFormat);
		JLabel vegetationInputLabel = new JLabel("Enter Vegetation Density");
		JTextField vegetationDensityInputField = new JFormattedTextField(decimalFormat);
		JLabel householdInputLabel = new JLabel("Enter Household Density");
		JTextField householdDensityInputField = new JFormattedTextField(decimalFormat);
		JButton generateManualButton = new JButton("Generate Manual Scenario");
		
		houseIndexField2.setColumns(5);
		proximityInputField.setColumns(5);
		vegetationDensityInputField.setColumns(5); 		
		householdDensityInputField.setColumns(5); 
	
		Context context = ContextUtils.getContext(this);
		
		mapFileLocation.setFileFilter(new FileFilter() {
			private final String[] okFileExtensions = new String[] {"shp"};
			@Override
			public String getDescription() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean accept(File f) {
				for (String extension : okFileExtensions)
			    {
			      if (f.isFile() && f.getName().toLowerCase().endsWith(extension))
			      {
			        return true;
			      }
			      if(f.isDirectory()) {
			    	  return true;
			      }
			    }
			    return false;
			}
		});
		//select new map
		selectMapButton.addActionListener(new ActionListener()
	    {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = mapFileLocation.showOpenDialog(panel1);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					newMapPath = mapFileLocation.getSelectedFile().getAbsolutePath();
					selectedMapLabel.setText(String.format("<html><div WIDTH=%d>%s</div><html>", 100, (newMapPath)));
					
		        } else {
		            System.out.println("Open command cancelled by user." );
		        }
			}
	    });
		//Apply changes
		changeMapButton.addActionListener(new ActionListener()
	    {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ContextCreator.usingManualGeneratedMap = false;
				if (newMapPath != null && !newMapPath.equalsIgnoreCase("")) {
				System.out.println("Changing map from " + ContextCreator.getMapPath() + " to " + newMapPath);
				ContextCreator.setMapPath(newMapPath);
				ContextCreator.pDomesticBreedingSpot = Double.parseDouble(houseIndexField1.getText());
				RSApplication.getRSApplicationInstance().reset();
				RSApplication.getRSApplicationInstance().initSim();
				} else {
					JOptionPane.showMessageDialog(panel1, "No Map Specified!", "No Map Change", JOptionPane.ERROR_MESSAGE);
				}
			}
	    });
		//Manual Map Generation Action
		generateManualButton.addActionListener(new ActionListener()
	    {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ContextCreator.usingManualGeneratedMap = true;
				if(!proximityInputField.getText().isEmpty() || !vegetationDensityInputField.getText().isEmpty() 
						|| !householdDensityInputField.getText().isEmpty()){
					ContextCreator.pDomesticBreedingSpot = Double.parseDouble(houseIndexField2.getText());
					ContextCreator.proximity = Double.parseDouble(proximityInputField.getText());
					ContextCreator.vegetationDensity = Double.parseDouble(vegetationDensityInputField.getText());
					ContextCreator.householdDensity = Double.parseDouble(householdDensityInputField.getText());
					RSApplication.getRSApplicationInstance().reset();
					RSApplication.getRSApplicationInstance().initSim();
				} else {
					JOptionPane.showMessageDialog(panel1, "Proximity must be in meters, while density values must be real numbers between 0 and 1", "Input Format Error!",  JOptionPane.ERROR_MESSAGE);
				}
			}
	    });
		setHouseIndexLabel1.setText("New House Index");
		setHouseIndexLabel1.setAlignmentX(0);
		setHouseIndexLabel2.setText("New House Index");
		setHouseIndexLabel2.setAlignmentX(0);

		
		panel1.add(logo);
		panel1.add(loadMapFromFileLabel);
		panel1.add(selectMapButton);
		panel1.add(selectNewMapLabel);
		panel1.add(selectedMapLabel);
		panel1.add(setHouseIndexLabel1);
		panel1.add(houseIndexField1);
		panel1.add(changeMapButton);
		panel1.add(manualMapGenerationLabel);
		panel1.add(setHouseIndexLabel2);
		panel1.add(houseIndexField2);
		panel1.add(proximityInputLabel);
		panel1.add(proximityInputField);
		panel1.add(vegetationInputLabel);
		panel1.add(vegetationDensityInputField);
		panel1.add(householdInputLabel);
		panel1.add(householdDensityInputField);
		panel1.add(generateManualButton);
		
		
		layout.putConstraint(SpringLayout.NORTH, logo, 50, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, loadMapFromFileLabel, 170, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, selectMapButton, 200, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, selectNewMapLabel, 250, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, selectedMapLabel, 270, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, setHouseIndexLabel1, 300, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, houseIndexField1, 300, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, changeMapButton, 350, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, manualMapGenerationLabel, 470, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, setHouseIndexLabel2, 500, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, houseIndexField2, 500, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, proximityInputLabel, 550, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, proximityInputField, 550, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, vegetationInputLabel, 600, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, vegetationDensityInputField, 600, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, householdInputLabel, 650, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, householdDensityInputField, 650, SpringLayout.NORTH, panel1);
		layout.putConstraint(SpringLayout.NORTH, generateManualButton, 700, SpringLayout.NORTH, panel1);
		
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, logo, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, loadMapFromFileLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, selectMapButton, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.WEST, selectNewMapLabel, 50, SpringLayout.WEST, panel1);
		layout.putConstraint(SpringLayout.WEST, selectedMapLabel, 50, SpringLayout.WEST, panel1);
		//layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, selectedMapLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.EAST, setHouseIndexLabel1, -10, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.WEST, houseIndexField1, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, changeMapButton, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, manualMapGenerationLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.EAST, setHouseIndexLabel2, -10, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.WEST, houseIndexField2, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.EAST, proximityInputLabel, -10, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.WEST, proximityInputField, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.EAST, vegetationInputLabel, -10, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.WEST, vegetationDensityInputField, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.EAST, householdInputLabel, -10, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.WEST, householdDensityInputField, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, generateManualButton, 0, SpringLayout.HORIZONTAL_CENTER, panel1);
		
		layout.putConstraint(SpringLayout.EAST, selectedMapLabel, 50, SpringLayout.EAST, panel1);
		return panel1;
	}

}


/*
JPanel panel1 = new JPanel();
panel1.setLayout( new BoxLayout(panel1, BoxLayout.Y_AXIS));
JButton wolbachiaButton = new JButton("add Wolbachia");
JTextField wolbachiaCountField = new JTextField(30);

//wolbachiaButton.action(new Event(wolbachiaButton, EventT, arg2), what)
//Context context = ContextUtils.getContext(this); 
// add the listener to the jbutton to handle the "pressed" event
wolbachiaButton.addActionListener(new ActionListener()
{
 
@Override
public void actionPerformed(ActionEvent arg0) {
	// TODO Auto-generated method stub
	int wolbachiaCount = Integer.valueOf(wolbachiaCountField.getText());
	//((PopulationMonitor) context.getObjects(PopulationMonitor.class).get(0)).infect(wolbachiaCount);
}
});
panel1.add(wolbachiaCountField);
panel1.add(wolbachiaButton);


JButton gmoButton = new JButton("add GMOs");
JTextField gmoCountField = new JTextField(30);

gmoButton.addActionListener(new ActionListener()
{
 
@Override
public void actionPerformed(ActionEvent arg0) {
	// TODO Auto-generated method stub
//	Parameters parm = RunEnvironment.getInstance().getParameters();
//	//int initialWolbachia = (Integer)parm.getValue("initialWolbachia");
//	int gmoCount = Integer.valueOf(gmoCountField.getText());
//	double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
//	System.out.println(temperature);
//		
//	char sex = 'f';
//	for (int i = 0; i < gmoCount; i++) {
//		int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
//		BreedingSpot birthPlace = (BreedingSpot) context.getObjects(BreedingSpot.class).get(index);
//		if(i > 5)
//			sex = 'm';
//		AedesAegypti agent = new AedesAegypti(birthPlace.x,birthPlace.y,sex,null,temperature, context);
//		agent.setBirthPlace(birthPlace);			
//		//just age them into adults and add
//		agent.setAsRandomlyAgedAdult();
//		agent.setLifeStage(LifeStages.ADULT);
//		agent.emerged = true;
//		agent.setDominantLethalGene(true);
//		context.add(agent);
//		((ContinuousSpace)context.getProjection("Space")).moveTo(agent, birthPlace.x,birthPlace.y);
//	}
}
});

panel1.add(gmoCountField);
panel1.add(gmoButton);
return panel1;*/