/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
 *  @author ezequielgioia
*/
package zikaVectorControl.vector;

import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;




import java.util.concurrent.RejectedExecutionException;

import com.vividsolutions.jts.geom.GeometryFactory;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.grid.GridWithin;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.GisAgent;
import zikaVectorControl.Host;
import zikaVectorControl.zones.BreedingSpot;
import zikaVectorControl.zones.CO2Source;
import zikaVectorControl.zones.Vegetation;
import zikaVectorControl.controllers.PopulationMonitor;
import zikaVectorControl.controllers.ThreadPool;
import zikaVectorControl.vector.AedesAegypti;


public class Mosquito extends Vector {

	Parameters parm;
	double distanceToTarget;
	/**
	 * mosquitoes have four life stages
	 * 
	 * @param human
	 */
	public enum LifeStages {
		EGG(0), LARVA(1), PUPA(2), ADULT(3);
		private final int stageID;

		LifeStages(int stageID) {
			this.stageID = stageID;
		}

		public int getStageID() {
			return stageID;
		}
	}
	protected static int threadID = 0;
	public int threadIDOfThis;
	LifeStages lifeStage;
	private boolean fetal = false;
	double fetalDuration;
	double eggDuration;
	double pupalDuration;
	double larvalDuration;
	double fetalMortality;
	double adultDuration;
	double adultMortality;
	private boolean dead = false;
	private int age;
	private int ticksAlive = 0;
	BreedingSpot birthPlace;
	private double pEmergence;
	private boolean emerged = false;
	/**
	 * reproduction parameters
	 */
	char sex;
	double eggsPerBatch;
	int durationOfOvipositioning;
	int timeSpentOvipositioning;
	double pMate;
	boolean fertilized;
	boolean eggsLaid;
	public Mosquito mate;
	int matesPerMosquito;
	BreedingSpot closestBreedingSpot;
	double pMaleChild;
	int timesMatedToday;
	boolean mateHadWolbachia = false;
	boolean mateWasRIDL = false;
	/**
	 * Living Habits
	 */
	float urbanPreference;
	double daytime; // ticks in a day
	/**
	 * movement variables
	 */
	private ModeOfMovement modeOfMovement;
	private double movementSpeedHigh;
	private double movementSpeedLow;
	private double currentSpeed;
	private boolean excited;
	private double vegetationDetected[]; // left,right,center-forward
	private double humidityDetected[]; // left,right,center-forward
	private double potentialMatesDetected[]; // left,right,center-forward
	private CO2Source closestCO2Source;
	protected Object target;
	GeometryFactory fac = new GeometryFactory();
	/**
	 * 
	 * Feeding variables
	 */
	private boolean fed;
	private int foodLevel = 0;

	private final int foodThreshold = 1;
	private double pSuccessfulFeed;
	private double pDieByHuman;

	public enum BehaviorMode {
		RANDOM, KLINOTAXIS, LANDSCAPE_FEATURE_FOLLOWING, MIGRATION_WITH_WIND, STATIONARY
	}

	enum lifeProcesses {
		FETAL, FOOD_SEEKING, FOOD_ENCOUNTERED, RESTING, OVIPOSITIONING, MATE_SEEKING
	}

	protected BehaviorMode currentBehavior;
	protected lifeProcesses currentProcess;

	private static ExecutorService service = Executors.newFixedThreadPool(16); //thread-related
	
	public Mosquito(int x, int y, char sex, Host human) {
		super(x, y);
		if (threadID == Runtime.getRuntime().availableProcessors() - 1) {
			threadID = 0;
		} else {
			threadID ++;
		}
		threadIDOfThis = threadID;
		parm = RunEnvironment.getInstance().getParameters();
		this.daytime = parm.getInteger("ticksPerDay");
		this.sex = sex;
		this.setHost(human);
		this.setModeOfMovement(Vector.ModeOfMovement.FLYING);
		this.currentBehavior = BehaviorMode.RANDOM;
		this.currentSpeed = this.getMovementSpeedHigh();
		this.timeSpentOvipositioning = 0;
	}

	
	/**
	 * This method performs an incremental radius search for closest neighbor
	 * @param <T>
	 * @param <T>
	 * @param maxRadius
	 * @return
	 */
	private  <T> Object getTarget(Class<T> type, double maxRadius) {
		Context context = ContextUtils.getContext(this);
		Object target = null;
		double searchRadius = 0;
		while(searchRadius < maxRadius){
			GridWithin<Object> within = new GridWithin<Object>(context, this, searchRadius);
			for (Object obj : within.query()) {
				if (type.isInstance(obj)) {
					if(!(obj instanceof Mosquito) || (obj instanceof Mosquito && ((Mosquito) obj).getSex() == 'f'
							&& !((Mosquito) obj).fertilized )) {
						target = type.cast(obj);
						break;
					}
				}
			}
			if(target != null ) {
				break;
			}
			searchRadius ++;
		}
		return target;
	}
	
	/**
	 * @return 1/8 of the day the tick falls in
	 */
	public int sectorOfDay() {
		double time = RunEnvironment.getInstance().getCurrentSchedule()
				.getTickCount();
		int sectorOfDay = (int) Math.floor(time % daytime / (daytime / 8));
		return sectorOfDay;
	}

	public boolean isMatingTime() {
		int sector = sectorOfDay();
		int searchResult = Arrays.binarySearch(matingTimeSectors(), sector);
		return searchResult != -1;
	}
	public int[] matingTimeSectors() {
		return new int[] {2,5};
	}
	public boolean isFeedingTime() {
		int sector = sectorOfDay();
		return isMatingTime() || sector == 3 || sector ==6 ;
	}

	public void updateLifeProcess() {
		if (lifeStage != LifeStages.ADULT) {
			currentProcess = lifeProcesses.FETAL;
			// hang around in the breeding site and possibly die
		} else {
			if (!fed) {
				if (isFeedingTime()) {
					if (currentProcess != lifeProcesses.FOOD_ENCOUNTERED) {
						currentProcess = lifeProcesses.FOOD_SEEKING;
					} else {
						currentProcess = lifeProcesses.FOOD_ENCOUNTERED;
					}
				} else {
					currentProcess = lifeProcesses.RESTING;
				}
			} else {
				// has to be fed to do these
				if (isMatingTime()) {
					// MatingCycle
					if (sex == 'f') {
						if (eggsLaid || !fertilized) {
							// unfertilized female can rest
							currentProcess = lifeProcesses.RESTING;
						} else {
							// if fertilized and mating time go lay eggs
							currentProcess = lifeProcesses.OVIPOSITIONING;
						}
					} else {
						currentProcess = lifeProcesses.MATE_SEEKING;
					}
				} else {
					// Already fed so rest
					currentProcess = lifeProcesses.RESTING;
				} 
			}
		}
	}

	public void updateBehavior() {
		switch (currentProcess) {
		case FOOD_SEEKING:
		case MATE_SEEKING:
		case OVIPOSITIONING:
			currentBehavior = BehaviorMode.KLINOTAXIS;
			if (sex == 'f' && closestCO2Source == null) {
				//setCurrentBehavior(BehaviorMode.RANDOM);
			}
//			if (sex == 'm' && closestVegetation == null) {
//				setCurrentBehavior(BehaviorMode.RANDOM);
//			}
			break;
		case FOOD_ENCOUNTERED:
		case RESTING:
			double p = Math.random();
			if (p < 0.5) {
				currentBehavior = BehaviorMode.STATIONARY;
			} else {
				currentBehavior = BehaviorMode.RANDOM;
			}
		case FETAL:
			currentBehavior = BehaviorMode.STATIONARY;
			break;
		default:
			currentBehavior = BehaviorMode.RANDOM;
			break;
		}
	}

	public void fly() {
		if (this.lifeStage == LifeStages.ADULT && !dead) {
			// update speed if excited or not excited
			if (excited && currentSpeed <= movementSpeedHigh) {
				currentSpeed++;
			} else if (currentSpeed > movementSpeedLow) {
				currentSpeed--;
			}
			Context context = ContextUtils.getContext(this);
			Grid<GisAgent> grid = (Grid<GisAgent>) context.getProjection("Grid");

			updateLifeProcess();
			updateBehavior();

			if (this.currentBehavior == BehaviorMode.RANDOM) {
				double noise = 0.6;
				grid.moveByDisplacement(this, (int)Math.round(Math.random() * noise), (int)Math.round(Math.random() * noise));
			}
			Parameters parm = RunEnvironment.getInstance().getParameters();
			double co2Radius = (Double) parm.getValue("co2Radius"); // meters
			double mateDiscoveryRadius = (Double) parm.getValue("mateDiscoveryRadius"); // meters
			double vegetationRadius = (Double) parm.getValue("vegetationRadius"); // meters
			if (this.currentBehavior == BehaviorMode.KLINOTAXIS) {
				// double sensors[] = new double[]{0,0,0};
				if (getCurrentProcess() == lifeProcesses.FOOD_SEEKING) {
					if (sex == 'f') {
						// sensors = this.getCo2LevelsDetected();
						target =  (CO2Source) getTarget(CO2Source.class, co2Radius);
						//if (target == null)
							//System.out.println("Co2"); 
					} else {
						// sensors = this.getVegetationDetected();
						target = (Vegetation) getTarget(Vegetation.class, vegetationRadius);
						//if (target == null)
							//System.out.println("Veg");
					}
				} else if (getCurrentProcess() == lifeProcesses.MATE_SEEKING) {
					if (sex == 'f') {
						// sensors = this.getHumidityDetected();
						System.out.println("invalid state");
					} else {
						// sensors = this.getPotentialMatesDetected();
						target = (Mosquito) getTarget(Mosquito.class, mateDiscoveryRadius);
						
						//System.out.println(((Mosquito)target).age);
					}
				} else if (getCurrentProcess() == lifeProcesses.OVIPOSITIONING) {
					if (sex == 'f') {
						// sensors = this.getHumidityDetected();
						target = (BreedingSpot) getTarget(BreedingSpot.class, vegetationRadius);
						//if (target == null)
							//System.out.println("breed");
					}
				}
				
				}
			if (this.currentBehavior == BehaviorMode.STATIONARY) {
				if (getCurrentProcess() == lifeProcesses.FOOD_ENCOUNTERED || !fed) {
					feed();
				}
			}
		}

	}
	
	public void moveToTarget () {
		if (this.lifeStage == LifeStages.ADULT && !dead && target != null  ) {
			// move
			Context context = ContextUtils.getContext(this);
			Grid<GisAgent> grid = (Grid<GisAgent>) context.getProjection("Grid");
			distanceToTarget = 0;
			if (
					((getCurrentProcess() == lifeProcesses.MATE_SEEKING && target instanceof Mosquito && !((Mosquito) target).dead)) 
					||
					(getCurrentProcess() == lifeProcesses.OVIPOSITIONING && target instanceof BreedingSpot)
					|| (getCurrentProcess() != lifeProcesses.MATE_SEEKING && getCurrentProcess() != lifeProcesses.OVIPOSITIONING)
			) {
				distanceToTarget = grid.getDistance(grid.getLocation(this), grid.getLocation(target));
				if (distanceToTarget < currentSpeed) { 
					grid.moveTo(this, grid.getLocation(target).getX(),grid.getLocation(target).getY());
				} else {
					System.out.println("stepwise movement");
				}
				distanceToTarget = grid.getDistance(grid.getLocation(this), grid.getLocation(target));
			}	
			if (getCurrentProcess() == lifeProcesses.FOOD_SEEKING && distanceToTarget <= 0) {
				currentProcess = lifeProcesses.FOOD_ENCOUNTERED;
				feed();
			} else if (getCurrentProcess() == lifeProcesses.MATE_SEEKING && target instanceof Mosquito && sex == 'm' && distanceToTarget <= 0) {
				this.mate = (Mosquito) target;
				mate();
			} else if (getCurrentProcess() == lifeProcesses.OVIPOSITIONING && target instanceof BreedingSpot && distanceToTarget <= 0) {
				closestBreedingSpot = (BreedingSpot) target;
				eggsLaid = oviposition();
			}
		}
	}

	public void feed() {
		if (lifeStage == LifeStages.ADULT) {
			double q = Math.random();
			double pFemaleGetsBloodMeal = 0.9/ daytime;
			if ( q < pFemaleGetsBloodMeal) {
				foodLevel++;
				if (foodLevel > foodThreshold) {
					fed = true;
				}
			}
			if(sex == 'f'){
				double p = Math.random();
				if(p < pDieByHuman / daytime){
					ContextUtils.getContext(this).remove(this);
					dead = true;
				}
			}
		}
	}

	public void mate() {
		//males perform this method
		if (lifeStage == LifeStages.ADULT) {
			// check males prob to mate
			double p = Math.random();
			if (p < pMate) {
				// female fertilized
				if (sex == 'm' && timesMatedToday < matesPerMosquito && mate.fed) {
					mate.fertilized = true;
					mate.eggsLaid = false;
					if(this instanceof AedesAegypti)
					mate.mateHadWolbachia = ((AedesAegypti)this).isWolbachiaInfected();
					mate.mateWasRIDL = ((AedesAegypti)this).isDominantLethalGene();
					timesMatedToday ++ ;
				}
				foodLevel -= foodThreshold / matesPerMosquito;
			}
		}
	}

	/**
	 * override this
	 */
	public boolean oviposition() {
		if (lifeStage == LifeStages.ADULT) {
			// female fertilized
			if (sex == 'f') {
				fertilized = false;
				fed = false;
				eggsLaid = true;
			}
		}
		System.out.println("oviposition unimplemented");
		return false;
	}

	/**
	 * grow old and die due to natural causes
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = ScheduleParameters.FIRST_PRIORITY)
	public void ageAndMortality() {
		
		LifeStages prevLifeStage = this.lifeStage;
		if (age < fetalDuration) {
			if(age < eggDuration) {
				lifeStage = LifeStages.EGG;	
			} else if (age < eggDuration + larvalDuration) {
				lifeStage = LifeStages.LARVA;
			} else if (age < eggDuration + larvalDuration + pupalDuration) {
				lifeStage = LifeStages.PUPA;
			}
			currentProcess = lifeProcesses.FETAL;
			double p = Math.random();
			if (p < ( fetalMortality /daytime )) {
				//die();
			}
		}else if (age >= fetalDuration) {
			lifeStage = LifeStages.ADULT;
			double p = Math.random();
			if (currentProcess != lifeProcesses.OVIPOSITIONING && p < adultMortality /daytime) {
				//die();
			}
		} 
		if (lifeStage == LifeStages.ADULT && currentProcess != lifeProcesses.OVIPOSITIONING && age > fetalDuration + adultDuration) {
			// adults die by old age
			die();
		}
		
		if(prevLifeStage != lifeStage) {
			if(!dead && lifeStage == LifeStages.ADULT){
				//kill off by fetal mortality
				double p = Math.random();
				if (p < ( fetalMortality /daytime )) {
					die();
				} else {
				this.emerge();
				}
			}
		}
		ticksAlive++;
		if (ticksAlive % daytime == 0 && ticksAlive >= daytime) {
			age++;
			timesMatedToday = 0;
		}
		if(lifeStage != LifeStages.ADULT) 
			fetal = true;
	}
	public void die(){
		if(dead)
			return;
		Context context = ContextUtils.getContext(this);
		context.remove(this);
		if(this.lifeStage == LifeStages.ADULT)
			((PopulationMonitor)context.getObjects(PopulationMonitor.class).get(0)).incrementDeaths((int) (this.getAge() - fetalDuration));
		dead = true;
		if (!isEmerged() ) {
			this.birthPlace.killOne(this);
		}		
	}
	public void emerge() {
		//kill emerging pupa
		double p = 0;// Math.random();
		if (p < pEmergence){
			die();
		} else {
			Context context = ContextUtils.getContext(this);
			Grid<GisAgent> grid = (Grid) context.getProjection("Grid");
			this.birthPlace.hatch(this);
			grid.moveTo(this, this.birthPlace.getX(),this.birthPlace.getY());
			fed = false;
		}
	}
	// ------------------------------------Getters
	// Setters-------------------------------------------------------------
	public void setCo2LevelsDetected(double co2LevelDetected[]) {
	}

	public double[] getVegetationDetected() {
		return vegetationDetected;
	}

	public void setVegetationDetected(double[] vegetationDetected) {
		this.vegetationDetected = vegetationDetected;
	}

	public double[] getHumidityDetected() {
		return humidityDetected;
	}

	public void setHumidityDetected(double[] humidityDetected) {
		this.humidityDetected = humidityDetected;
	}

	public double[] getPotentialMatesDetected() {
		return potentialMatesDetected;
	}

	public void setPotentialMatesDetected(double[] potentialMatesDetected) {
		this.potentialMatesDetected = potentialMatesDetected;
	}

	public double getMovementSpeedHigh() {
		return movementSpeedHigh;
	}

	public void setMovementSpeedHigh(double movementSpeedHigh) {
		this.movementSpeedHigh = movementSpeedHigh * ((24 * 3600) / daytime)
				* 1;
	}

	public ModeOfMovement getModeOfMovement() {
		return modeOfMovement;
	}

	public void setModeOfMovement(ModeOfMovement modeOfMovement) {
		this.modeOfMovement = modeOfMovement;
	}

	public double getMovementSpeedLow() {
		return movementSpeedLow;
	}

	public void setMovementSpeedLow(double movementSpeedLow) {
		this.movementSpeedLow = movementSpeedLow * ((24 * 3600) / daytime) * 1;
	}

	public double getCurrentSpeed() {
		return currentSpeed;
	}

	public void setCurrentSpeed(double currentSpeed) {
		this.currentSpeed = currentSpeed;
	}

	public BehaviorMode getCurrentBehavior() {
		return currentBehavior;
	}

	public void setCurrentBehavior(BehaviorMode currentBehavior) {
		this.currentBehavior = currentBehavior;
	}

	public LifeStages getLifeStage() {
		return lifeStage;
	}

	public void setLifeStage(LifeStages lifeStage) {
		this.lifeStage = lifeStage;
	}

	
	public double getFetalMortality() {
		return fetalMortality;
	}


	public void setFetalMortality(double fetalMortality) {
		this.fetalMortality = fetalMortality;
	}


	public double getEggsPerBatch() {
		
		return eggsPerBatch;
	}

	public void setEggsPerBatch(double eggsPerBatch) {
		this.eggsPerBatch = eggsPerBatch;
	}

	public double getpSuccessfulFeed() {
		return pSuccessfulFeed;
	}

	public void setpSuccessfulFeed(double pSuccessfulFeed) {
		this.pSuccessfulFeed = pSuccessfulFeed;
	}

	public double getpDieByHuman() {
		return pDieByHuman;
	}

	public void setpDieByHuman(double pDieByHuman) {
		this.pDieByHuman = pDieByHuman;
	}

	public double getpMate() {
		return pMate;
	}

	public void setpMate(double pMate) {
		this.pMate = pMate;
	}

	public char getSex() {
		return sex;
	}

	public boolean isFertilized() {
		return fertilized;
	}

	public void setFertilized(boolean fertilized) {
		this.fertilized = fertilized;
	}

	public void setLifeCycleDurations(double eggDuration2, double larvalDuration2,
			double pupalDuration2, double adultDuration2) {
		double p = Math.random() *2;
		this.fetalDuration =  eggDuration2+p+ larvalDuration2+pupalDuration2;
		this.eggDuration = eggDuration2;
		this.larvalDuration = larvalDuration2;
		this.pupalDuration = pupalDuration2;
		this.adultDuration = adultDuration2;
	}
	
	public double getFetalDuration() {
		return fetalDuration;
	}

	public void setFetalDuration(double fetalDuration) {
		this.fetalDuration = fetalDuration;
	}

	public void setAdultDuration(double adultDuration) {
		double p = Math.random() *2;
		this.adultDuration = (double) (adultDuration + p);
	}
	public double getAdultDuration() {
		return this.adultDuration;
	}

	public double getAdultMortality() {
		return adultMortality;
	}


	public void setAdultMortality(double adultMortality) {
		this.adultMortality = adultMortality;
	}

	public lifeProcesses getCurrentProcess() {
		return currentProcess;
	}

	public void setCurrentProcess(lifeProcesses currentProcess) {
		this.currentProcess = currentProcess;
	}

	public boolean isFed() {
		return fed;
	}

	public void setFed(boolean fed) {
		this.fed = fed;
	}

	public int getFoodLevel() {
		return foodLevel;
	}

	public void setFoodLevel(int foodLevel) {
		this.foodLevel = foodLevel;
	}

	public int getMatesPerMosquito() {
		return matesPerMosquito;
	}

	public void setMatesPerMosquito(int matesPerMosquito) {
		this.matesPerMosquito = matesPerMosquito;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public BreedingSpot getBreedingSpot() {
		return closestBreedingSpot;
	}

	public void setBreedingSpot(BreedingSpot breedingSpot) {
		this.closestBreedingSpot = breedingSpot;
	}

	public double getpMaleChild() {
		return pMaleChild;
	}

	public void setpMaleChild(double pMaleChild) {
		this.pMaleChild = pMaleChild;
	}

	public boolean isFetal() {
		return fetal;
	}

	public void setFetal(boolean fetal) {
		this.fetal = fetal;
	}

	public BreedingSpot getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(BreedingSpot birthPlace) {
		this.birthPlace = birthPlace;
	}
	public int getDurationOfOvipositioning() {
		return durationOfOvipositioning;
	}

	public void setDurationOfOvipositioning(int durationOfOvipositioning) {
		this.durationOfOvipositioning = durationOfOvipositioning;
	}


	public double getpEmergence() {
		return pEmergence;
	}


	public void setpEmergence(double pEmergence) {
		this.pEmergence = pEmergence;
	}


	public boolean isEmerged() {
		return emerged;
	}


	public void setEmerged(boolean emerged) {
		this.emerged = emerged;
	}
	
}
