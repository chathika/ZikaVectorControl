/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
*/
package zikaVectorControl.controllers;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.AedesAegypti;
import zikaVectorControl.vector.Mosquito.LifeStages;
import zikaVectorControl.zones.CO2Source;
public class WolbachiaReleaseController {
	
	
	int releases = 1;
	//5040 is july  using april as custom
		//@ScheduledMethod(start = 20160, interval = 180, priority = ScheduleParameters.FIRST_PRIORITY)
		public void infect1() {
			Parameters parm = RunEnvironment.getInstance().getParameters();
			//int initialWolbachia = (Integer)parm.getValue("initialWolbachia");
			Context context = ContextUtils.getContext(this);
			double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
			int numUrbanPoints = context.getObjects(CO2Source.class).size();
			System.out.println(numUrbanPoints);
			//numbers from Nguyen et al. 2015 Machan's Beach study
			// release 11 males and 11 females a week per urban point
			int maleCount = 11; 
			int femaleCount = 11;
			float freqReleasePoints = 0.25f;
			System.out.println(freqReleasePoints);
			//int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
			if(releases > 15)
				return;
			for (int i = 0; i < numUrbanPoints * freqReleasePoints; i++) {
				int index = Math.round((int)Math.random() * (context.getObjects(CO2Source.class).size() - 1));
				CO2Source ReleasePoint = (CO2Source) context.getObjects(CO2Source.class).get(index); 
				for (int j = 0; j < maleCount; j++) {
					char sex = 'm';
					AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
					agent.setBirthPlace(null);			
					//just age them into adults and add
					agent.setAge((int) (agent.getFetalDuration()+1));
					agent.setLifeStage(LifeStages.ADULT);
					agent.setEmerged(true);
					agent.setWolbachiaInfected(true,false);
					context.add(agent);
					((Grid)context.getProjection("Grid")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());
				}
				for (int j = 0; j < femaleCount; j++) {
					char sex = 'f';
					AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
					agent.setBirthPlace(null);			
					//just age them into adults and add
					agent.setAge((int) (agent.getFetalDuration()+1));
					agent.setLifeStage(LifeStages.ADULT);
					agent.setEmerged(true);
					agent.setWolbachiaInfected(true,false);
					context.add(agent);
					((Grid)context.getProjection("Grid")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());}
				}
			releases++;
		}
		//@ScheduledMethod(start = 21960, interval = 180, priority = ScheduleParameters.FIRST_PRIORITY)
		public void infect2() {
			Parameters parm = RunEnvironment.getInstance().getParameters();
			//int initialWolbachia = (Integer)parm.getValue("initialWolbachia");
			Context context = ContextUtils.getContext(this);
			double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
			int numUrbanPoints = context.getObjects(CO2Source.class).size();
			System.out.println(numUrbanPoints);
			//numbers from Nguyen et al. 2015 Machan's Beach study
			// release 11 males and 11 females a week per urban point
			int maleCount = 6; 
			int femaleCount = 0;
			float freqReleasePoints = 0.25f;
			System.out.println(freqReleasePoints);
			//int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
			if(releases > 10)
				return;
			for (int i = 0; i < numUrbanPoints * freqReleasePoints; i++) {
				int index = Math.round((int)Math.random() * (context.getObjects(CO2Source.class).size() - 1));
				CO2Source ReleasePoint = (CO2Source) context.getObjects(CO2Source.class).get(index); 
				for (int j = 0; j < maleCount; j++) {
					char sex = 'm';
					AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
					agent.setBirthPlace(null);			
					//just age them into adults and add
					agent.setAge((int) (agent.getFetalDuration()+1));
					agent.setLifeStage(LifeStages.ADULT);
					agent.setEmerged(true);
					agent.setWolbachiaInfected(true,false);
					context.add(agent);
					((Grid)context.getProjection("Grid")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());
				}
				for (int j = 0; j < femaleCount; j++) {
					char sex = 'f';
					AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
					agent.setBirthPlace(null);			
					//just age them into adults and add
					agent.setAge((int) (agent.getFetalDuration()+1));
					agent.setLifeStage(LifeStages.ADULT);
					agent.setEmerged(true);
					agent.setWolbachiaInfected(true,false);
					context.add(agent);
					((Grid)context.getProjection("Grid")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());}
				}
			releases++;
		}
}
