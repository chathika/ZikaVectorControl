/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
*/
package zikaVectorControl.controllers;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.AedesAegypti;
import zikaVectorControl.vector.Mosquito;

public class ClimateController {
	
	private int month;
	double temperature;
	private double fetalDurationAtThisTemperature;
	private double eggDurationAtThisTemperature;
	
	private double larvalDurationAtThisTemperature;
	private double pupalDurationAtThisTemperature;
	
	private double adultDurationAtThisTemperature;
	private double fetalMortalityAtThisTemperature;
	private double adultMortalityRateAtThisTemperature;
	private double ovipositionRate;
	
	double eggMortalityRateAtThisTemperature;
	double larvalMortalityRateAtThisTemperature;
	double pupalMortalityRateAtThisTemperature;
	
	
	public ClimateController() {
		this.month = 1;
		
	}
	/**
	 * This function will run every month to:
	 * - Update the temperature of the mosquitoes
	 * - Update the mortality rates according to (Otero, 2006) 
	 * 
	 * Additionally, this function starts after 10000 ticks (reasonable time to initialize normal behavior in sim
	 */
	@ScheduledMethod(start = 1, interval = 720, priority = ScheduleParameters.FIRST_PRIORITY)
	public void controlClimate (){
		
		temperature = 293.706;
		double monthlyTemperature[] = {293.706, 293.45556, 298.06667, 300.01111, 300.56667, 
				301.9, 303.06667, 303.0389, 302.5389, 300.9, 299.56667, 298.4833};
		
		temperature = monthlyTemperature[month-1];
		
		month++;
		if(month > 12) 
			month = 1;
		double tempInC = this.temperature - 273.15;
		
//		eggMortalityRateAtThisTemperature = 0.01;
//		larvalMortalityRateAtThisTemperature = 0.01 + 0.9725*Math.exp(-(temperature - 278)/2.7035);
//		pupalMortalityRateAtThisTemperature = 0.01 + 0.9725*Math.exp(-(temperature - 278)/2.7035);
		
		//adult mortality, adult duration, and oviposition rate change with temperature
		fetalMortalityAtThisTemperature = 0.5412630  -0.0445754*tempInC + 0.0009371*Math.pow(tempInC,2);
		adultMortalityRateAtThisTemperature = 0.1897 -0.010330*tempInC + 0.0002718*Math.pow(tempInC,2);
		fetalDurationAtThisTemperature = 217.79077 -14.17510*tempInC + 0.23573*Math.pow(tempInC,2);
		adultDurationAtThisTemperature = -30.82832 +5.28638*tempInC -0.11095*Math.pow(tempInC,2);
		ovipositionRate = 22.763452 -3.861519*tempInC + 0.203669*Math.pow(tempInC,2) -0.003017*Math.pow(tempInC,3); 
		
		eggDurationAtThisTemperature = 205.262256 -20.927773*tempInC + 0.714711*Math.pow(tempInC, 2) - 0.008079*Math.pow(tempInC, 3);
		larvalDurationAtThisTemperature = 198.115170 - 20.513954*tempInC + 0.721462*Math.pow(tempInC, 2) - 0.008348 * Math.pow(tempInC, 3);
		pupalDurationAtThisTemperature = 24.8356730 - 2.0691341*tempInC + 0.0626787*Math.pow(tempInC, 2) - 0.0006472 * Math.pow(tempInC, 3);
		
		
		System.out.println(eggDurationAtThisTemperature + " " + fetalDurationAtThisTemperature + " " + larvalDurationAtThisTemperature + " " + pupalDurationAtThisTemperature + " " + adultDurationAtThisTemperature);
		Context context = ContextUtils.getContext(this);
		for (Object m : context.getObjects(Mosquito.class)) {
			((AedesAegypti) m).updateFetalMortalityRateByTemperature(temperature);
			((AedesAegypti) m).updateAdultMortalityRateByTemperature(temperature);
		}
	}
	
	public double getEggDurationAtThisTemperature() {
		return eggDurationAtThisTemperature;
	}
	public double getPupalDurationAtThisTemperature() {
		return pupalDurationAtThisTemperature;
	}
	public double getLarvalDurationAtThisTemperature() {
		return larvalDurationAtThisTemperature;
	}
	public double getEggMortalityRateAtThisTemperature() {
		return eggMortalityRateAtThisTemperature;
	}
	public void setEggMortalityRateAtThisTemperature(
			double eggMortalityRateAtThisTemperature) {
		this.eggMortalityRateAtThisTemperature = eggMortalityRateAtThisTemperature;
	}
	public double getLarvalMortalityRateAtThisTemperature() {
		return larvalMortalityRateAtThisTemperature;
	}
	public void setLarvalMortalityRateAtThisTemperature(
			double larvalMortalityRateAtThisTemperature) {
		this.larvalMortalityRateAtThisTemperature = larvalMortalityRateAtThisTemperature;
	}
	public double getPupalMortalityRateAtThisTemperature() {
		return pupalMortalityRateAtThisTemperature;
	}
	public void setPupalMortalityRateAtThisTemperature(
			double pupalMortalityRateAtThisTemperature) {
		this.pupalMortalityRateAtThisTemperature = pupalMortalityRateAtThisTemperature;
	}
	public double getAdultDurationAtCurrentTemperature() {
		return adultDurationAtThisTemperature;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public double getTemperature() {
		return temperature;
	}
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	public double getFetalMortalityRateAtThisTemperature() {
		return fetalMortalityAtThisTemperature;
	}
	public double getAdultMortalityRatesAtThisTemperature() {
		return adultMortalityRateAtThisTemperature;
	}
	public double getOvipositionRateAtThisTemperature() {
		return ovipositionRate;
	}
	public double getFetalDurationThisTemperature() {
		return fetalDurationAtThisTemperature;
	}
}
