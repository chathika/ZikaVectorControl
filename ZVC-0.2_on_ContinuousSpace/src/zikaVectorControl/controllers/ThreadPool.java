package zikaVectorControl.controllers;

import java.util.ArrayList;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.Mosquito;

public class ThreadPool {
	private ArrayList<ZVCThread> threads;
	public static int threadCount = Runtime.getRuntime().availableProcessors();
	public ThreadPool () {
		System.out.println("Distributing agent processes over " + threadCount + " on this node");
	}
	
	@ScheduledMethod(start = 1,   interval = 1, priority =4)
	public void execute () {
		threads = new ArrayList();
		for(int i = 0; i < threadCount; i++)
			threads.add(new ZVCThread());
		Context context = ContextUtils.getContext(this);
		for(Object mosquito: context.getObjects(Mosquito.class)) {
			threads.get(((Mosquito)mosquito).threadIDOfThis).mosquitoes.add((Mosquito)mosquito);
		}
		for(ZVCThread thread: threads) {
			System.out.print(thread.mosquitoes.size() + " ");
			thread.start();
		}
		System.out.println();
		for(ZVCThread thread: threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	class ZVCThread extends Thread{
		ArrayList<Mosquito> mosquitoes;
		public ZVCThread (){
			mosquitoes = new ArrayList<Mosquito>();
		}
		@Override
		public void run() {
			for(Mosquito mosquito: mosquitoes){
				if(mosquito != null) {
					mosquito.fly();
				} else {
					mosquitoes.remove(mosquito);
				}
			}
		}	
	}
}
