package zikaVectorControl;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

import repast.simphony.space.gis.GISAdder;
import repast.simphony.space.gis.Geography;
public class LocatedAdder<T> implements GISAdder<Object> {
	
	
	/*public void addCO2(Grid<Object> grid, int x, int y, int radius){
		Context context = ContextUtils.getContext(this);
		Geography<Object> geography = (Geography)context.getProjection("Geography");
		
		
		CO2Source source = new CO2Source(x,y,radius);
		//grid.getCellAccessor().put(source, grid, source.getGp());
		MooreQuery<Object> query = new MooreQuery<Object>(grid, (Object)source, radius);
		
		
		for (Object o : query.query()) {
			if(o instanceof CO2Trail) {
				grid.getCellAccessor().remove(o, grid, grid.getLocation(o));
			}
		}		
		
		for(int i=0; i<radius; i++){
			for(int j=0; j<radius; j++){
				GridPoint tgp = new GridPoint(source.getX()+(i-(radius/2)), source.getY()+(j-(radius/2)));
				grid.getCellAccessor().put(new CO2Trail(source), grid, tgp);
			}
		}		
	}*/
	@Override
	public void add(Geography<Object> destination, Object object) {
		if(object instanceof GisAgent) {
			Double x = new Double(((GisAgent)object).getX());
			Double y = new Double(((GisAgent)object).getY());
			GeometryFactory fac = new GeometryFactory();
			Coordinate coord = new Coordinate(((GisAgent) object).getX(), ((GisAgent) object).getY());
			Point geom = fac.createPoint(coord);
			destination.move(object, geom);
		}
	}
}